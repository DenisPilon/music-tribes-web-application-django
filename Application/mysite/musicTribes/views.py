import json
import logging
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.urls import reverse

from musicTribes.models import Playlist, Tribe, TribeMember, Message, Track, Like, Comment
from .forms import JoinForm, PlaylistForm, TribeForm, MessageForm, TrackForm, CommentForm

def index(request):
    tribes = Tribe.objects.order_by('-id')
    if(request.user.is_authenticated):
        chieftainTribes = Tribe.tribesChieftain_for(request.user)
        myTribes= TribeMember.tribes_for(request.user)
        notMyTribes= TribeMember.available_tribes_for(request.user)
        context = { 'tribes': tribes,
            'myTribes': myTribes,
            'notMyTribes': notMyTribes,
            'chieftainTribes': chieftainTribes}
    else:
         context = { 'tribes': tribes}
    return render(request, 'app/index.html', context)


def create_tribe(request):
    if request.method=='POST':
        form = TribeForm(request.POST)
        if form.is_valid():
            saved_tribe=form.save(commit=False)
            saved_tribe.chieftain=request.user
            saved_tribe.save()
            return HttpResponseRedirect(reverse('MusicTribes:detail', args=(saved_tribe.id,)))
    else:
        form=TribeForm()
    context={'form':form, 'action': 'create'}
    return render(request, 'app/create_tribe.html',context)

def detail(request, tribe_id):
    tribe = get_object_or_404(Tribe,pk=tribe_id)
    playlists = Playlist.objects.order_by('-id')
    tribeMembers = TribeMember.objects.all()
    messages = Message.objects.all()
    if(request.user.is_authenticated):
        myTribes= TribeMember.tribes_for(request.user)
        context = {'myTribes': myTribes,
                    'tribe': tribe,
                    'playlists': playlists,
                    'tribeMembers': tribeMembers,
                    'messages': messages}
    else:
        context = {'tribe': tribe,
                    'playlists': playlists,
                    'tribeMembers': tribeMembers,
                    'messages': messages}
    return render(request, 'app/detail.html', context)

def join_tribe(request, tribe_id):
     if request.method=='POST':
        form = JoinForm(request.POST)
        if form.is_valid():
            saved_tribe=form.save(commit=False)
            saved_tribe.member=request.user
            saved_tribe.save()
            return HttpResponseRedirect(reverse('MusicTribes:index'))

def add_playlist(request, tribe_id):
    tribe = get_object_or_404(Tribe,pk=tribe_id)
    if request.method=='POST':
        form = PlaylistForm(request.POST)
        if form.is_valid():
            saved_playlist=form.save(commit=False)
            saved_playlist.tribe=tribe
            saved_playlist.creator=request.user
            saved_playlist.save()
            return HttpResponseRedirect(reverse('MusicTribes:detail', args=(tribe.id,)))
    else:
        form=PlaylistForm()
    context={'form':form, 'action': 'create'}
    return render(request, 'app/add_playlist.html',context)

def delete_playlist(request, playlist_id):
    playlist=get_object_or_404(Playlist, pk=playlist_id)
    if request.method=='POST' and request.user.is_authenticated:
        playlist.delete()
    return HttpResponseRedirect(reverse('MusicTribes:detail', args=(playlist.tribe.id,)))

def edit_playlist(request, playlist_id):
    playlist=get_object_or_404(Playlist, pk=playlist_id)
    if request.method=='POST':
        form = PlaylistForm(request.POST, instance=playlist)
        if form.is_valid():
            saved_playlist=form.save()
            return HttpResponseRedirect(reverse('MusicTribes:detail', args=(saved_playlist.tribe.id,)))
    else:
        form=PlaylistForm(instance=playlist)
    context={'form':form, 'action':'update'}
    return render(request, 'app/add_playlist.html',context)

def kick_user(request, tribeMember_id):
    tribeMember=get_object_or_404(TribeMember, pk=tribeMember_id)
    if request.method=='POST' and request.user.is_authenticated:
        tribeMember.delete()
    return HttpResponseRedirect(reverse('MusicTribes:detail', args=(tribeMember.tribe.id,)))

def add_message(request, tribe_id):
    tribe= get_object_or_404(Tribe, pk=tribe_id)
    if request.method=='POST':
        form = MessageForm(request.POST)
        if form.is_valid():
            saved_message=form.save(commit=False)
            saved_message.tribe=tribe
            saved_message.sender= request.user
            saved_message.save()
            return HttpResponseRedirect(reverse('MusicTribes:detail', args=(tribe.id,)))

def delete_message(request, message_id, tribe_id):
    messsage=get_object_or_404(Message, pk=message_id)
    tribe=get_object_or_404(Tribe, pk=tribe_id)
    if request.method=='POST' and request.user == tribe.chieftain:
        messsage.delete()
    return HttpResponseRedirect(reverse('MusicTribes:detail', args=(messsage.tribe.id,)))

def playlist_detail(request, playlist_id, tribe_id):
    playlist = get_object_or_404(Playlist,pk=playlist_id)
    tracks = Track.objects.filter(playlist=playlist)
    tribe= get_object_or_404(Tribe, pk=tribe_id)
    likes=[track.like_by(request.user) for track in tracks]
    comments = Comment.objects.all()
    if(request.user.is_authenticated): 
        myTribes= TribeMember.tribes_for(request.user)
        context = {'tracks_with_likes': zip(tracks,likes),
                    'myTribes': myTribes,
                    'tribe': tribe,
                    'playlist': playlist,
                    'comments': comments}
    else:
        context = {'playlist': playlist,
                    'tribe': tribe,
                    'tracks_with_likes': zip(tracks,likes),
                    'comments': comments}
    return render(request, 'app/playlist_detail.html', context)

def add_track(request, tribe_id, playlist_id):
    tribe = get_object_or_404(Tribe,pk=tribe_id)
    playlist = get_object_or_404(Playlist,pk=playlist_id)
    if request.method=='POST':
        form = TrackForm(request.POST)
        if form.is_valid():
            saved_track=form.save(commit=False)
            saved_track.playlist=playlist
            saved_track.user=request.user
            saved_track.save()
            return HttpResponseRedirect(reverse('MusicTribes:playlist_detail', args=(playlist.id, tribe.id)))
    else:
        form=TrackForm()
    context={'form':form, 'action': 'create'}
    return render(request, 'app/add_track.html',context)

def like_track(request, track_id):
    track=get_object_or_404(Track, pk=track_id)
    if request.user.is_authenticated:
        like= Like.objects.filter(user=request.user, track=track).first()
        if like:
            like.delete()
            return HttpResponseRedirect(reverse('MusicTribes:playlist_detail', args=(track.playlist.id, track.playlist.tribe.id)))
        else:
            like=Like(user=request.user, track=track, value=True)
        try:
            like.full_clean()
            like.save()
        except:
            return HttpResponseRedirect(reverse('MusicTribes:playlist_detail', args=(track.playlist.id, track.playlist.tribe.id)))
        else:
            return HttpResponseRedirect(reverse('MusicTribes:playlist_detail', args=(track.playlist.id, track.playlist.tribe.id)))
    else:
        return HttpResponseRedirect(reverse('MusicTribes:playlist_detail', args=(track.playlist.id, track.playlist.tribe.id)))

def add_comment(request, track_id):
    track=get_object_or_404(Track, pk=track_id)
    if request.method == 'POST'and request.user.is_authenticated:
        form=CommentForm(request.POST)
        if form.is_valid():
            comment=form.save(commit=False)
            comment.track=track
            comment.user= request.user
            comment.save()
            return HttpResponseRedirect(reverse('MusicTribes:playlist_detail', args=(track.playlist.id, track.playlist.tribe.id)))
        else:
            return HttpResponseRedirect(reverse('MusicTribes:playlist_detail', args=(track.playlist.id, track.playlist.tribe.id)))
    else:
         return HttpResponseRedirect(reverse('MusicTribes:playlist_detail', args=(track.playlist.id, track.playlist.tribe.id)))

def delete_track(request, track_id):
    track=get_object_or_404(Track, pk=track_id)
    if request.method=='POST' and request.user.is_authenticated:
        track.delete()
    return HttpResponseRedirect(reverse('MusicTribes:playlist_detail', args=(track.playlist.id, track.playlist.tribe.id)))

def delete_comment(request, track_id, comment_id):
    track=get_object_or_404(Track, pk=track_id)
    comment=get_object_or_404(Comment, pk=comment_id)
    if request.method=='POST':
        comment.delete()
    #return HttpResponseRedirect(reverse('MusicTribes:index'))
    return HttpResponseRedirect(reverse('MusicTribes:playlist_detail', args=(track.playlist.id, track.playlist.tribe.id)))