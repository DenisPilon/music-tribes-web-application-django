# Generated by Django 3.1.3 on 2022-05-02 12:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('musicTribes', '0049_playlist_timestamp'),
    ]

    operations = [
        migrations.CreateModel(
            name='Track',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(editable=False)),
                ('updated_at', models.DateTimeField(editable=False)),
                ('artist', models.CharField(max_length=1200)),
                ('title', models.CharField(max_length=1200)),
                ('url', models.CharField(max_length=1200)),
                ('duration', models.CharField(max_length=1200)),
                ('playlist', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='playlist', to='musicTribes.playlist')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
