# Generated by Django 3.1.3 on 2022-02-25 11:20

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('musicTribes', '0006_auto_20220224_1904'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tribe',
            name='members',
        ),
    ]
