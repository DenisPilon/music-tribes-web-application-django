# Generated by Django 3.1.3 on 2022-03-05 10:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('musicTribes', '0039_auto_20220228_2049'),
    ]

    operations = [
        migrations.AddField(
            model_name='tribe',
            name='member',
            field=models.JSONField(default=1),
            preserve_default=False,
        ),
    ]
