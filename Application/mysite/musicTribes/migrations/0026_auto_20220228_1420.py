# Generated by Django 3.1.3 on 2022-02-28 13:20

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('musicTribes', '0025_auto_20220228_1414'),
    ]

    operations = [
        migrations.RenameField(
            model_name='tribemember',
            old_name='user',
            new_name='member',
        ),
        migrations.AlterField(
            model_name='tribemember',
            name='tribe',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='musicTribes.tribe'),
        ),
    ]
