from xml.etree.ElementTree import Comment
from django import forms
from django.contrib import admin
from .models import Like, Message, Tribe, TribeMember, Playlist, Track, Like, Comment
from django.contrib.auth.models import User


# Register your models here.
admin.site.register(Tribe)
admin.site.register(TribeMember)
admin.site.register(Playlist)
admin.site.register(Message)
admin.site.register(Track)
admin.site.register(Like)
admin.site.register(Comment)

