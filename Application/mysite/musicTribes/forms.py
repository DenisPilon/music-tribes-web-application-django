from tkinter import Place
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from musicTribes.models import Playlist, Tribe, TribeMember, Message, Track, Comment

class UserForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ('username', 'email', 'password1' ,'password2' )

class Edit_User_Form(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ('username', 'email')

class TribeForm(forms.ModelForm):
    class Meta:
        model=Tribe
        fields=['tribeName','tribeLogo','genre']

class JoinForm(forms.ModelForm):
    class Meta:
        model=TribeMember
        fields=['tribe','member']

class PlaylistForm(forms.ModelForm):
    class Meta:
        model=Playlist
        fields = ['name', 'description']
        
class MessageForm(forms.ModelForm):
    class Meta:
        model=Message
        fields = ['message']

class TrackForm(forms.ModelForm):
    class Meta:
        model=Track
        fields = ['artist', 'title', 'url', 'duration']

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields=['text']
      

