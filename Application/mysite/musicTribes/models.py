from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


# Create your models here.
class TimeStamped(models.Model):
    created_at = models.DateTimeField(editable=False)
    updated_at = models.DateTimeField(editable=False)

    def save(self, *args, **kwargs):
        if not self.created_at:
            self.created_at = timezone.now()

        self.updated_at = timezone.now()
        return super(TimeStamped, self).save(*args, **kwargs)

    class Meta:
        abstract = True

class Tribe(TimeStamped):
    tribeName = models.CharField(max_length=128, unique=True, blank=False)
    tribeLogo = models.ImageField(default='defaultTribeLogo.jpg', upload_to='tribeLogo_pics')
    genre = models.CharField(max_length=128, blank=False)
    chieftain=models.ForeignKey(User, on_delete=models.CASCADE, related_name="username_users") 

    @classmethod
    def tribesChieftain_for(cls, user):
        chieftainTribe=Tribe.objects.filter(chieftain=user)
        return chieftainTribe

    def __str__(self):
        return self.tribeName

class TribeMember(TimeStamped):
    tribe = models.ForeignKey(Tribe, on_delete=models.CASCADE, related_name="tribe", unique=False) 
    member= models.ForeignKey(User, on_delete=models.CASCADE, related_name="member", unique=False) 

    @classmethod
    def tribes_for(cls, user):
        memberships=TribeMember.objects.filter(member=user)
        myTribes=[x.tribe for x in memberships]
        return myTribes

    @classmethod
    def available_tribes_for(cls, user):
        myTribes=TribeMember.tribes_for(user)
        tribes= Tribe.objects.all()
        availableTribes=[tribe for tribe in tribes if tribe not in myTribes and user != tribe.chieftain]
        return availableTribes

    def __str__(self):
        return f"{self.member.username} becomes member of {self.tribe.tribeName} tribe"

class Playlist(TimeStamped):
    tribe = models.ForeignKey(Tribe, on_delete=models.CASCADE, unique=False)
    name = models.CharField(max_length=128, unique=True, blank=False)
    description = models.TextField()
    creator = models.ForeignKey(User, on_delete=models.CASCADE, related_name='creator')
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"Tribe {self.tribe}: playlist {self.name}"

class Message(TimeStamped):
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name='sender')
    tribe = models.ForeignKey(Tribe, on_delete=models.CASCADE, related_name='receiver')
    message = models.CharField(max_length=1200)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.sender.username} wrote a message in {self.tribe.tribeName} tribe"

class Track(TimeStamped):
    playlist = models.ForeignKey(Playlist, on_delete=models.CASCADE, related_name="playlist", unique=False) 
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    artist = models.CharField(max_length=1200)
    title = models.CharField(max_length=1200)
    url = models.CharField(max_length=1200)
    duration = models.CharField(max_length=1200)
    likes=models.IntegerField(default=0)
    comments=models.IntegerField(default=0)

    def like_score(self):
        return self.like_set.filter(value=True).count()
    
    def comment_score(self):
        return self.comment_set.count()
    
    def like_by(self, user):
        if user.is_authenticated:
            return Like.objects.filter(user=user, track=self).first()
        else:
            return None


    def __str__(self):
        return f"{self.title} track added to {self.playlist.name} playlist"

class Like(TimeStamped):
    track = models.ForeignKey(Track, on_delete=models.CASCADE, unique=False) 
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    value= models.BooleanField(null=False, default=False)

    class Meta:
        constraints=[
            models.UniqueConstraint(name='user_like',fields=['track','user'])
        ]

    def __str__(self):
        return f"{self.user.username} liked  track {self.track.title}"

class Comment(TimeStamped):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    track = models.ForeignKey(Track, on_delete=models.CASCADE)
    text = models.CharField(max_length=1200)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.user.username} wrote a comment on track {self.track.title}"