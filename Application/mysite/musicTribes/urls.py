from django.urls import path

from . import views

app_name='MusicTribes'
urlpatterns = [
    path('', views.index, name='index'),
    path('create_tribe', views.create_tribe, name="create_tribe"),
    path('<int:tribe_id>/', views.detail, name='detail'),
    path('<int:tribe_id>/join_tribe', views.join_tribe, name="join_tribe"),
    path('<int:tribe_id>/add_playlist', views.add_playlist, name="add_playlist"),
    path('<int:playlist_id>/delete_playlist', views.delete_playlist, name="delete_playlist"),
    path('<int:playlist_id>/edit_playlist', views.edit_playlist, name="edit_playlist"),
    path('<int:tribeMember_id>/kick_user', views.kick_user, name="kick_user"),
    path('<int:tribe_id>/add_message', views.add_message, name="add_message"),
    path('<int:message_id>/<int:tribe_id>/delete_message', views.delete_message, name="delete_message"),
    path('<int:playlist_id>/<int:tribe_id>/playlist_detail', views.playlist_detail, name='playlist_detail'),
    path('<int:tribe_id>/<int:playlist_id>/add_track', views.add_track, name="add_track"),
    path('like_track/<int:track_id>', views.like_track, name="like_track"),
    path('<int:track_id>/add_comment', views.add_comment, name='add_comment'),
    path('<int:track_id>/delete_track', views.delete_track, name="delete_track"),
    path('<int:comment_id>/<int:track_id>/delete_comment', views.delete_comment, name="delete_comment"),
]