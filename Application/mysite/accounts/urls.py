from django import urls
from django.urls import path

from . import views

app_name="accounts"
urlpatterns=[
    path('sign-up', views.SignUpView.as_view(), name='signup'),
    path('profile', views.profile, name='profile')
] 
