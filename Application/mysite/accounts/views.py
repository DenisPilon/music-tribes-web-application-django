from audioop import reverse
from django.contrib.auth import login, authenticate
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render, get_object_or_404


# Create your views here.
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.views import generic
from accounts.forms import ProfileUpdateForm, UserUpdateForm
from musicTribes.forms import UserForm, Edit_User_Form
from django.contrib.auth.decorators import login_required




class SignUpView(generic.CreateView):
    form_class= UserForm
    success_url= reverse_lazy('index')
    template_name='registration/signup.html'

    def form_valid(self, form):
        valid = super(SignUpView, self).form_valid(form)
        username= form.cleaned_data.get('username')
        password= form.cleaned_data.get('password1')
        new_user = authenticate(self.request, username=username, password=password)
        login(self.request, new_user)
        return valid
        
# Update it here
@login_required
def profile(request):
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST,
                                   request.FILES,
                                   instance=request.user.profile) 
        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            return redirect('/accounts/profile') # Redirect back to profile page

    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)

    context = {
        'u_form': u_form,
        'p_form': p_form
    }

    return render(request, 'accounts/edit_user.html', context)